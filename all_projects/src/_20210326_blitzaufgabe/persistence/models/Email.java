package _20210326_blitzaufgabe.persistence.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class Email{

    public Email(String email){
        this.email = email;
        newMail("s.stanislaus@tsn.at", 1);
        newMail("s.stanislaus@gmail.com", 1);
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}