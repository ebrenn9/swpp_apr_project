package _20210326_blitzaufgabe.persistence.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class User{

    public User(String user){
        this.user = user;
        newUser("Stanislaus");
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }
}

