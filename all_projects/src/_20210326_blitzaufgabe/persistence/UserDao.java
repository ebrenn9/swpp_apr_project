package _20210326_blitzaufgabe.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDao{

static class DbController{
public static void newUser(String name){
    Connection conn = null;
    String url = "jdbc:sqlite:./all_projects/src/_20210326_blitzaufgabe/mydb.db";
    newUser("Stanislaus");
    
    try {
        conn = DriverManager.getConnection(url);
    } catch (SQLException e1) {
        e1.printStackTrace();
    }

    String sql = "INSERT INTO users(name) VALUES(?)";

    try {
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, name);
        pstmt.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
    }

    try {
        conn.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }

}
}

public static void createNewTable() {

    Connection conn = null;

    try {
        conn = DriverManager.getConnection(url);
    } catch (SQLException e1) {
        e1.printStackTrace();
    }

    String sql_persons = "CREATE TABLE IF NOT EXISTS users (\n"
            + "	id integer not null PRIMARY KEY,\n"
            + "	name varchar(255)\n"
            + ");";
    try {
        Statement stmt = conn.createStatement();
        stmt.execute(sql_persons);
        System.out.println("Table persons is created!");
    } catch (SQLException e) {
        e.printStackTrace();
    }

    String sql_emails = "CREATE TABLE IF NOT EXISTS mails (\n"
            + "	id integer not null PRIMARY KEY,\n"
            + "	address varchar(255),\n"
            + "	person_id int\n"
            + ");";
    try {
        Statement stmt = conn.createStatement();
        stmt.execute(sql_emails);
        System.out.println("Table emails is created!");
    } catch (SQLException e) {
        e.printStackTrace();
    }

    try {
        conn.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
}