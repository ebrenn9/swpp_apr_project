package _20210205_DAO_mysql_notReady_ThreadScope.persistence.models;

public class Monster {

    private String race;
    private String power;

public Monster(String race, String power){
    this.race = race;
    this.power = power;
    }

    public String getRace(){
        return race;
    }
    
    public void setRace(String race){
        this.race = race;
    }

    public String getPower(){
        return power;
    }
    
    public void setPower(String power){
        this.power = power;
    }

    public String toString(){
        return getRace()+" - "+getPower();
    }
}