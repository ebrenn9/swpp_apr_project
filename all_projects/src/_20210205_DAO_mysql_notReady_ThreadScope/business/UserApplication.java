package _20210205_DAO_mysql_notReady_ThreadScope.business;

import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.User;
import _20210205_DAO_mysql_notReady.persistence.Dao;
import _20210205_DAO_mysql_notReady.persistence.DaoFactory;
import _20210205_DAO_mysql_notReady.persistence.DaoType;
import _20210205_DAO_mysql_notReady.persistence.models.Monster;

public class UserApplication {

    private static Dao<User> userDao;
    private static Dao<Monster> monsterDao;
    public static void main(String[] args) {
        userDao = DaoFactory.getDao(DaoType.USER);
        monsterDao = DaoFactory.getDao(DaoType.MONSTER);

        User user1 = getUser(0);
        System.out.println("Erste Ausgabe: "+user1);
        userDao.update(user1, new String[]{"Jake", "jake@domain.com"});


        userDao.delete(1);
        userDao.save(new User("Julie", "julie@domain.com"));

        userDao.getAll().forEach(user -> System.out.println(user));

        Monster monster1 = getMonster(0);
        System.out.println("Erste Ausgabe: "+monster1);
        monsterDao.update(monster1, new String[]{"Grizzly", "100"});

        monsterDao.delete(1);
        monsterDao.save(new Monster("Yeti", "80"));

        monsterDao.getAll().forEach(monster -> System.out.println(monster));
    }

    private static User getUser(int id) {
        Optional<User> user = userDao.get(id);
        
        return user.orElse(
          new User("non-existing user", "no-email"));
    }
    
    private static Monster getMonster(int id){
        Optional<Monster> monster = monsterDao.get(id);

        return monster.orElse(
            new Monster("non-existing monster", "no-power"));
    }
}