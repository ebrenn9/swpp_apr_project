package _20210205_DAO_mysql_notReady_MethodScope.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.User;

public class UserDao implements Dao<User> {
    
    public UserDao() {

        DbController.createUserTable();        

        this.save(new User("John", "john@domain.com"));
        this.save(new User("Susan", "susan@domain.com"));
    }
    
    @Override
    public Optional<User> get(int id) {
        return Optional.of(DbController.giveone(id));
    }
    
    @Override
    public List<User> getAll() {
    return DbController.giveAll();
    }
    
    @Override
    public void save(User user) {
        DbController.insertUser(user.getName(), user.getEmail());
    }
    
    @Override
    public void update(User user, String[] params) {

        user.setName(Objects.requireNonNull(
          params[0], "Name cannot be null"));
        user.setEmail(Objects.requireNonNull(
          params[1], "Email cannot be null"));
        
        // von Tutorial korrigiert, dass User nicht doppelt in Liste
        //users.add(user);
    }
    
    @Override
    public void delete(int id) {
        DbController.delete(id);
    }
}

class DbController {
    private static String url = "jdbc:sqlite:./all_projects/src/_20210205_DAO_mysql_notReady_MethodScope/mydb.db";

    public static void insertUser(String name, String email){
        String sql = "INSERT INTO users(name,email) VALUES(?,?)";
        try {
            Connection conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public static void createUserTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Connection conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static User giveone(int id){
        String sql = "SELECT name, email FROM users";
        ArrayList<User> users = new ArrayList<User>();

        try{
            Connection conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                User user = new User(rs.getString("name"), rs.getString("email"));
                users.add(user);
            }
            conn.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return users.get(id);
    }

    public static ArrayList<User> giveOne(int id){
        String sql = "SELECT name, email FROM users";
        ArrayList<User> users = new ArrayList<User>();

        try{
            Connection conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                User user = new User(rs.getString("name"), rs.getString("email"));
                users.add(user);
            }
            conn.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    public static ArrayList<User> giveAll(){
        String sql = "SELECT name, email FROM users";
        ArrayList<User> users = new ArrayList<User>();

        try{
            Connection conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                User user = new User(rs.getString("name"), rs.getString("email"));
                users.add(user);
            }
            conn.close();
        } catch (SQLException e){
            e.printStackTrace();
        } 
        return users;
    }

    public static void delete(int id){
        String sql = "Delete from users where id = ?";
        try{
            Connection conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            conn.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
