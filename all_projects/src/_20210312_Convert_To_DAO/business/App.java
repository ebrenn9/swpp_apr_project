package _20210312_Convert_To_DAO.business;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import _20210312_Convert_To_DAO.OrderDao;

public class App {    

    public static void main(String[] args) {
        OrderDao order = new OrderDao();
        order.createNewTable();
        order.newOrder("Thönie", "2021-03-11");
        order.connect();
    }
   

