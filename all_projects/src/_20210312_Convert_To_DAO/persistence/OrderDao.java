package _20210312_Convert_To_DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


//InstanceScope
public class OrderDao{    

    private static String url = "jdbc:sqlite:./all_projects/src/_20210312_Convert_To_DAO/mydb.db";

    public static void newOrder(String customer, String order_date){
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

       String sql = "INSERT INTO orders(customer,order_date) VALUES(?,?)";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, customer);
            pstmt.setString(2, order_date);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createNewTable() {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String sql = "CREATE TABLE IF NOT EXISTS orders (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	order_date date NOT NULL,\n"
                + "	customer varchar(255)\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void connect() {
        String url = "jdbc:sqlite:./all_projects/src/_20210312_Convert_To_DAO/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }
}
